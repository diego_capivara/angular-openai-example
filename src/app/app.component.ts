import { Component, OnInit } from '@angular/core';
import { OpenAiService, openAIMessages } from './open-ai.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'openAi';
  content!: string;

  constructor(
    private openAi: OpenAiService
  ) {}

  ngOnInit(): void {
    const messages: openAIMessages[] = [
      {
        role: 'user',
        content: 'Como passar item por item em um array no javascript?'
      },
      {
        role: 'system',
        content: 'Você é um programador javascript, explique detalhadamente para uma pessoa que não sabe nada de javascript e faça ela se sentir bem. Comece a resposta elogiando a pergunta e depois resolvendo a questão.'
      }
    ];

    this.openAi.ask(messages).subscribe((result: any) => {
      console.log('result', result);
      this.content = result.message.content;
    });
  }
}
