import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface openAIMessages {
  role: 'user' | 'system' | 'assistant' | 'System';
  content: string;
}

@Injectable({
  providedIn: 'root'
})
export class OpenAiService {

  constructor(
    private httpClient: HttpClient
  ) { }

  ask(messages: openAIMessages[]) {
    return this.httpClient.post('http://localhost:3050/ask', { messages });
  }

}
